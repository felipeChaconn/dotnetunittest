﻿using ClassLibrary1.Controllers;
using ClassLibrary1.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;


namespace TestProyectoQA
{
    [TestClass]
    public class BusTest
    {

        
        [TestMethod]
        public void CreateBusTest_InsertRightValues()
        {
            //Arrange
            Bus bus = new Bus(250, "B0009", "Sergio Rojas", "Ronaldo Araya", 0);
            var busController = new BusController();
            //Act
            var resBus = busController.SaveBus(bus);
            //Assert
            Assert.IsNotNull(resBus);
            // Assert.AreEqual(resBus.Status, 1);
           }



            [TestMethod]
        public void UpdateBusById()
        {

            //Arrange
            Bus bus = new Bus(6,210, "B0007", "Juan Andres Rojas", "Orlando Galo", 1);
            var busController = new BusController();
            //Act
            var resBus = busController.UpdateBus(bus);
            //Assert
            Console.WriteLine(resBus.id);
            Assert.IsNotNull(resBus);
          //  Assert.AreEqual(resBus.id, 7);
        }



        [TestMethod]
        public void SearchBusTest_ByRealCode()
        {
            byte status = 1;
            //Arrange
            String Code = "B0006";
            var busController = new BusController();
            //Act
            var resBus = busController.SearchBusByCode(Code);
            //Assert
            Assert.IsNotNull(resBus.id);
            Assert.AreEqual(resBus.Status, status);
        }


    }
}
