﻿using ClassLibrary1.Controllers;
using ClassLibrary1.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;


namespace TestProyectoQA
{
    [TestClass]
    public class RouteTest
    {
        

        [TestMethod]
        public void CreateRouteTest_InsertRightValues()
        {
            byte status = 1;
            //Arrange
            Route route = new Route(1, "123", "Viajeros", 20, "Ciudad Quesada", 12, status);
            var routeController = new RouteController();
            //Act
            var resRoute = routeController.SaveRoute(route);
            //Assert
            Assert.IsNotNull(resRoute);
            Assert.AreEqual(route.Status, status);
        }



        [ExpectedException(typeof(ArgumentNullException),
          "EL bus del id no existe")]
        [TestMethod]
        public void CreateRouteTest_InsertBadValueInBusID()
        {
            //Arrange
            Route route = new Route(100, "122", "Chilsaca", 20, "Ciudad Quesada", 12, 1);
            var routeController = new RouteController();
            //Act
             routeController.SaveRoute(route);
            //Assert
        }

    }
}
