﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassLibrary1.Models;
using ClassLibrary1.Controllers;

namespace TestProyectoQA
{
    [TestClass]
    public class ScheduleTest
    {

        [TestMethod]
        public void CreateScheduleTest_InsertRightValues()
        {
            //Arrange
            byte status = 1;
            Schedule schedule = new Schedule(6, "S003", "Horario Fortuna-CQ", "5:00 AM", "6:00 AM", status);
            ScheduleController sc = new ScheduleController();
            //Act
            var resSc = sc.InsertSchedule(schedule);
            //Assert
            //Console.WriteLine(resSc.id);
            Assert.IsNotNull(resSc);
            Assert.AreEqual(resSc.Status, status);
        }

        [ExpectedException(typeof(Exception),
            "No se pudo agregar el horario.")]
        [TestMethod]
        public void CreateScheduleTest_InsertWrongCode()
        {
            //Arrange
            Schedule schedule = new Schedule(7, "S0006", "Horario Fortuna-CQ", "5:00 AM", "6:00 AM", 1);
            ScheduleController sc = new ScheduleController();
            //Act
            sc.InsertSchedule(schedule);
            //Assert

        }

        [TestMethod]
        public void SearchScheduleTest_ByRealCode()
        {
            //Arrange
            byte status = 1;
            String Code = "S002";
            var controller = new ScheduleController();
            //Act
            var res = controller.SearchScheduleByCode(Code);
            //Assert
            Assert.IsNotNull(res);
            Assert.AreEqual(res.Status, status);
        }

        [ExpectedException(typeof(Exception),
            "No existe el bus.")]
        [TestMethod]
        public void SearchScheduleTest_ByWrongCode()
        {
            //Arrange
            String Code = "S00100";
            var controller = new ScheduleController();
            //Act
            var res = controller.SearchScheduleByCode(Code);
            //Assert
        }
    }
}
