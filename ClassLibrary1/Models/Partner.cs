﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Models
{
    public class Partner
    {
        public int Bus_id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public int Line { get; set; }
        public bool Status { get; set; }
        public int id { get; set; }

        public Partner(int id, int bus_id, string code, string name,
            string lastname, int line, bool status)
        {
            this.id = id;
            this.Bus_id = bus_id;
            this.Code = code;
            this.Name = name;
            this.Lastname = lastname;
            this.Line = line;
            this.Status = status;
        }

        public Partner(int bus_id, string code, string name,
            string lastname, int line, bool status)
        {
            this.Bus_id = bus_id;
            this.Code = code;
            this.Name = name;
            this.Lastname = lastname;
            this.Line = line;
            this.Status = status;
        }

        public Partner()
        {

        }
    }
}
