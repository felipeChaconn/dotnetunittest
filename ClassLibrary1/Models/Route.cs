﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Models
{
    public class Route
    {
        public int Bus_id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int Delay_minutes { get; set; }
        public string Zone { get; set; }
        public int Quantity { get; set; }
        public byte Status { get; set; }
        public int id { get; set; }

        public Route(int id, int bus_id, string code, string description,
            int delay_minutes, string zone, int quantity, byte status)
        {
            this.id = id;
            this.Bus_id = bus_id;
            this.Code = code;
            this.Description = description;
            this.Delay_minutes = delay_minutes;
            this.Zone = zone;
            this.Quantity = quantity;
            this.Status = status;
        }

        public Route(int bus_id, string code, string description,
            int delay_minutes, string zone, int quantity, byte status)
        {
            this.Bus_id = bus_id;
            this.Code = code;
            this.Description = description;
            this.Delay_minutes = delay_minutes;
            this.Zone = zone;
            this.Quantity = quantity;
            this.Status = status;
        }

        public Route()
        {

        }
    }
}
