﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Models
{
    public class Bus
    {
        public int id { get; set; }
        public int Capacity { get; set; }
        public string Code { get; set; }
        public string Assistant { get; set; }
        public string Driver { get; set; }
        public byte Status { get; set; }

        public Bus(int id, int capacity, string code, string assistant, string driver, byte status)
        {
            this.id = id;
            this.Capacity = capacity;
            this.Code = code;
            this.Assistant = assistant;
            this.Driver = driver;
            this.Status = status;
        }

        public Bus(int capacity, string code, string assistant, string driver, byte status)
        {
            this.Capacity = capacity;
            this.Code = code;
            this.Assistant = assistant;
            this.Driver = driver;
            this.Status = status;
        }

        public Bus()
        {

        }
    }
}
