﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Models
{
    public class Schedule
    {
        public int id { get; set; }
        public int Bus_id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Depart_time { get; set; }
        public string Arrival_time { get; set; }
        public byte Status { get; set; }
        public Bus Bus { get; set; }


        public Schedule(int bus_id, string code, string description,
           string depart_time, string arrival_time, byte status, int id)
        {
            this.id = id;
            this.Bus_id = bus_id;
            this.Status = status;
        }


        public Schedule(int bus_id, string code, string description,
           string depart_time, string arrival_time, byte status)

        {
            this.Bus_id = bus_id;
            this.Code = code;
            this.Description = description;
            this.Depart_time = depart_time;
            this.Arrival_time = arrival_time;
            this.Status = status;
        }

        public Schedule()
        {

        }
    }
}
