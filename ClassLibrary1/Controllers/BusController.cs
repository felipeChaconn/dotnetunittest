﻿using ClassLibrary1.Models;
using ClassLibrary1.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Controllers
{
    public class BusController
    {

        public Bus SaveBus(Bus bus)
        {
            //Validaciones de nulos
            if (bus == null)
            {
                throw new ArgumentNullException("El bus es nulo");
            }else if (bus.Status < 0 && bus.Status > 1)
            {
                throw new ArgumentNullException("El estatus dek bus es nulo");
            }
            else if (bus.Driver == null)
            {
                throw new ArgumentNullException("El conductor es nulo");
            }
            else if (bus.Code == null || bus.Code.Trim() == "")
            {
                throw new ArgumentNullException("El código del bus es nulo.");
            }else if(bus.Capacity < 0)
            {
                throw new ArgumentNullException("Un bus no puede tener una capacidad de 0 personas o menos.");
            }else if (bus.Assistant == null)
            {

            }
            //Validaciones de N/A
            if (bus.Assistant.Trim() == "")
            {
                bus.Assistant = "N/A";
            }
            if (bus.Driver.Trim() == "")
            {
                bus.Driver = "N/A";
            }


            return new BusServer().Create(bus);
        }
        public Bus UpdateBus(Bus updatedBus)
        {
            if (updatedBus != null)
            {
                return new BusServer().Update(updatedBus);
               
            }
            return null;
           throw new ArgumentNullException("Debe digitar un id valido.");

        }
        public Bus SearchBusByCode(string Code)
        {
            if (Code == null || Code.Trim() == "")
            {
                throw new ArgumentNullException("El código del bus es nulo o vacío.");
            }

            return new BusServer().SearchByCode(Code.Trim());
        }
    }
}
