﻿using ClassLibrary1.Models;
using ClassLibrary1.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Controllers
{
    public class ScheduleController
    {

        public Schedule InsertSchedule(Schedule NewSchedule)
        {
            if (NewSchedule == null)
            {
                throw new ArgumentNullException("El horario es nulo");
            }
            else if (NewSchedule.Code == null || NewSchedule.Code.Trim() == "")
            {
                throw new ArgumentNullException("El código del horario es nulo o está vacío");
            }
            else if (NewSchedule.Description == null || NewSchedule.Description.Trim() == "")
            {
                throw new ArgumentNullException("La descripción del horario es nula o está vacía");
            }
            else if (NewSchedule.Depart_time == null || NewSchedule.Depart_time.Trim() == "")
            {
                throw new ArgumentNullException("La hora de partida del horario es nula o está vacía");
            }
            else if (NewSchedule.Arrival_time == null || NewSchedule.Arrival_time.Trim() == "")
            {
                throw new ArgumentNullException("La hora de llegada del horario es nula o está vacía");
            }
            else if (NewSchedule.Bus_id <= 0)
            {
                throw new ArgumentNullException("Se necesita un código de bus válido");
            }


            return new ScheduleServer().Create(NewSchedule);
        }

        public Schedule SearchScheduleByCode(string Code)
        {
            this.ValidateCode(Code);
            return new ScheduleServer().SearchByCode(Code.Trim());
        }

        public Schedule UpdateScheduleByCode(string Code)
        {
            this.ValidateCode(Code);
            return null;
        }

        private void ValidateCode(string Code)
        {
            if (Code == null || Code.Trim() == "")
            {
                throw new ArgumentNullException("El código del horario es nulo o vacío.");
            }
        }
    }
}
