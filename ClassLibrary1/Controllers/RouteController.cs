﻿using ClassLibrary1.Models;
using ClassLibrary1.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Controllers
{
    public class RouteController
    {
        public Route SaveRoute(Route route)
        {
            var busExists = new BusServer().SearchById(route.Bus_id); ;

            if (busExists == null)
            {
                throw new ArgumentNullException("EL bus del id no existe");
            }
            return new RouteServer().Create(route);

        }

        public Route SearchRouteByCode(string Code)
        {
            if (Code == null || Code.Trim() == "")
            {
                throw new ArgumentNullException("El código de la ruta es nulo o vacío.");
            }

            return new RouteServer().SearchByCode(Code.Trim());
        }

    }
}
