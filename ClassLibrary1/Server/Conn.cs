﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ClassLibrary1.Server
{
    public class Conn
    {
        private static string Server = "localhost";
        private static string BDName = "proyecto1";
        private static string Username = "userqa";
        private static string Password = "admin";
        private string ConnectionString = $"Server={Server};Database={BDName};User ID={Username}; Password={Password}";
        private SqlConnection Connection;

        /// <summary>
        /// Method to open a connection and return the Connection to be use it.
        /// </summary>
        /// <returns>SqlConnection method</returns>
        public SqlConnection openConnection()
        {
            Connection = new SqlConnection(this.ConnectionString);
            return Connection;
        }
    }
}
