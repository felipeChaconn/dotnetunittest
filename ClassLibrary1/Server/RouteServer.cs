﻿using ClassLibrary1.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Server
{
    class RouteServer
    {
        private Conn connClass;
        private SqlConnection con;
        private SqlCommand command;
        private SqlDataReader dataReader;
        private string sql = null;

        public RouteServer()
        {
            connClass = new Conn();
        }

        public Route Create(Route newRoute)
        {
           
                this.sql = $"INSERT into route (bus_id, code, description, delay_minutes, zone, quantity, status) OUTPUT Inserted.id values ('{newRoute.Bus_id}', '{newRoute.Description}', '{newRoute.Description}', '{newRoute.Delay_minutes}', '{newRoute.Zone}', '{newRoute.Quantity}',  '{newRoute.Status}');";
                this.con = this.connClass.openConnection();
                this.con.Open();
                try
                {
                    command = new SqlCommand(sql, this.con);
                    dataReader = command.ExecuteReader();
                    if (dataReader.Read())
                    {
                        newRoute.id = Convert.ToInt32(dataReader[0]);
                    }

                    dataReader.Close();
                    command.Dispose();

                    return newRoute;
                }
                catch (Exception ex)
                {
                    throw new Exception("No se pudo agregar el route.", ex);
                }
                finally
                {
                    this.con.Close();
                }
        }


        public Route SearchByCode(String Code)
        {
            Route route = null;

            this.sql = $"SELECT * from route where code = '{Code}';";
            this.con = this.connClass.openConnection();
            this.con.Open();
            try
            {
                command = new SqlCommand(sql, this.con);
                dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    route = new Route();
                    route.id = Convert.ToInt32(dataReader[0]);
                    route.Bus_id = Convert.ToInt32(dataReader[1]);
                    route.Code = dataReader[2].ToString();
                    route.Description = dataReader[3].ToString();
                    route.Delay_minutes = Convert.ToInt32(dataReader[4]);
                    route.Zone = dataReader[5].ToString();
                    route.Quantity = Convert.ToInt32(dataReader[6]);
                    route.Status = Convert.ToByte(dataReader[7]);
                }

                dataReader.Close();
                command.Dispose();
                return route;
            }
            catch (Exception ex)
            {
                throw new Exception("No existe la ruta.", ex);
            }
            finally
            {
                this.con.Close();
            }

        }

    }
}
