﻿using ClassLibrary1.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Server
{
    class ScheduleServer
    {
        private Conn connClass;
        private SqlConnection con;
        private SqlCommand command;
        private SqlDataReader dataReader;
        private string sql = null;

        public ScheduleServer()
        {
            connClass = new Conn();
        }

        public Schedule Create(Schedule schedule)
        {

            this.sql = $"insert into schedule ( bus_id, code, description, depart_time, arrival_time, status) OUTPUT Inserted.id values ('{schedule.Bus_id}','{schedule.Code}', '{schedule.Description}','{schedule.Depart_time}','{schedule.Arrival_time}','{schedule.Status}');";
            this.con = this.connClass.openConnection();
            this.con.Open();
            try
            {
                command = new SqlCommand(sql, this.con);
                dataReader = command.ExecuteReader();

                if (dataReader.Read())
                {
                    schedule.id = Convert.ToInt32(dataReader[0]);
                }

                dataReader.Close();
                command.Dispose();
                this.con.Close();
                return schedule;
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo agregar el horario.", ex);
            }
            finally
            {
                this.con.Close();
            }
        }

        public Schedule SearchByCode(string Code)
        {
            Schedule SearchedSchedule = null;

            this.sql = $"select s.id, s.code, bus_id, s.description, s.depart_time, s.arrival_time, s.status, b.id, b.code, b.capacity, b.assistant, b.driver, b.status from schedule s INNER JOIN bus b ON s.bus_id = b.id where s.code = '{Code}';";

            this.con = this.connClass.openConnection();
            this.con.Open();
            try
            {
                command = new SqlCommand(sql, this.con);
                dataReader = command.ExecuteReader();
                SearchedSchedule = new Schedule();
                if (dataReader.Read())
                {

                    SearchedSchedule.id = Convert.ToInt32(dataReader[0]);
                    SearchedSchedule.Code = dataReader[1].ToString();
                    SearchedSchedule.Bus_id = Convert.ToInt32(dataReader[2]);
                    SearchedSchedule.Description = dataReader[3].ToString();
                    SearchedSchedule.Depart_time = dataReader[4].ToString();
                    SearchedSchedule.Arrival_time = dataReader[5].ToString();
                    SearchedSchedule.Status = Convert.ToByte(dataReader[6]);
                    //Ahora el bus
                    SearchedSchedule.Bus = new Bus();
                    SearchedSchedule.Bus.id = Convert.ToInt32(dataReader[7]);
                    SearchedSchedule.Bus.Code = dataReader[8].ToString();
                    SearchedSchedule.Bus.Capacity = Convert.ToInt32(dataReader[9]);
                    SearchedSchedule.Bus.Assistant = dataReader[10].ToString();
                    SearchedSchedule.Bus.Driver = dataReader[11].ToString();
                    SearchedSchedule.Bus.Status = Convert.ToByte(dataReader[12]);
                }
                else
                {
                    this.con.Close();
                    throw new Exception("No existe el bus.");
                }

                dataReader.Close();
                command.Dispose();
                this.con.Close();
                return SearchedSchedule;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw new Exception("No existe el bus.", ex);
            }
            finally
            {
                this.con.Close();
            }
        }

        public Schedule Update(string Code, Schedule ScheduleToUpdate)
        {
            var actualSchedule = this.SearchByCode(Code);

            if (ScheduleToUpdate.Arrival_time != null && ScheduleToUpdate.Arrival_time != actualSchedule.Arrival_time)
            {
                actualSchedule.Arrival_time = ScheduleToUpdate.Arrival_time;
            }
            if (ScheduleToUpdate.Bus_id > 0 && ScheduleToUpdate.Bus_id != actualSchedule.Bus_id)
            {
                actualSchedule.Bus_id = ScheduleToUpdate.Bus_id;
            }
            if (ScheduleToUpdate.Depart_time != null && ScheduleToUpdate.Depart_time != actualSchedule.Depart_time)
            {
                actualSchedule.Depart_time = ScheduleToUpdate.Depart_time;
            }
            if (ScheduleToUpdate.Description != null && ScheduleToUpdate.Description != actualSchedule.Description)
            {
                actualSchedule.Description = ScheduleToUpdate.Description;
            }
            if (ScheduleToUpdate.Status >= 0 && ScheduleToUpdate.Status <= 1 && ScheduleToUpdate.Status != actualSchedule.Status)
            {
                actualSchedule.Status = ScheduleToUpdate.Status;
            }

            sql = $"UPDATE schedule SET bus_id = {actualSchedule.Bus_id}, description = '{actualSchedule.Description}', depart_time = '{actualSchedule.Depart_time}', arrival_time = '{actualSchedule.Arrival_time}' , status = {actualSchedule.Status} WHERE code = '{Code}';";
            this.con.Open();
            try
            {
                command = new SqlCommand(sql, con);
                var res = command.ExecuteNonQuery();

                if (res <= 0)
                {
                    throw new Exception("No se pudo actualizar el horario.");
                }

                dataReader.Close();
                command.Dispose();
                this.con.Close();
                return actualSchedule;
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo actualizar el horario.", ex);
            }
            finally
            {
                this.con.Close();
            }



        }
    }
}
