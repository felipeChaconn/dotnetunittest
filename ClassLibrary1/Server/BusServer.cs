﻿using ClassLibrary1.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Server
{
    class BusServer
    {
        private Conn connClass;
        private SqlConnection con;
        private SqlCommand command;
        private SqlDataReader dataReader;
        private string sql = null;

        public BusServer()
        {
            connClass = new Conn();
        }

        public Bus Create(Bus NewBus)
        {
            this.sql = $"insert into bus (code, capacity, assistant, driver, status) OUTPUT Inserted.id values ('{NewBus.Code}', '{NewBus.Capacity}', '{NewBus.Assistant}', '{NewBus.Driver}', '{NewBus.Status}');";
            this.con = this.connClass.openConnection();
            this.con.Open();
            try
            {
                command = new SqlCommand(sql, this.con);
                dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    NewBus.id = Convert.ToInt32(dataReader[0]);
                }

                dataReader.Close();
                command.Dispose();

                return NewBus;
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo agregar el bus.", ex);
            }
            finally
            {
                this.con.Close();
            }
        }

        public Bus Update(Bus updatedBus)
        {

            var newBus = this.SearchById(updatedBus.id);

            if (newBus != null)
            {
                this.sql = $"UPDATE  bus SET code = '{updatedBus.Code}',  capacity='{updatedBus.Capacity}',  assistant='{updatedBus.Assistant}',  driver='{updatedBus.Driver}', status='{updatedBus.Status}' WHERE id='{newBus.id}'";
                this.con = this.connClass.openConnection();
                this.con.Open();
                try
                {
                    command = new SqlCommand(sql, this.con);
                    dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        Console.WriteLine(dataReader.GetValue(0));
                    }

                    dataReader.Close();
                    command.Dispose();
                    return newBus;
                }
                catch (Exception ex)
                {
                    throw new Exception("No se pudo editar el bus.", ex);
                }
                finally
                {
                    this.con.Close();
                }
            }
            else
            {
                throw new Exception("No se encontro el bus");
            }

        }

        public Bus SearchByCode(String Code)
        {
            Bus bus = null;

            this.sql = $"SELECT * from bus where code = '{Code}';";
            this.con = this.connClass.openConnection();
            this.con.Open();
            try
            {
                command = new SqlCommand(sql, this.con);
                dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    bus = new Bus();
                    bus.id = Convert.ToInt32(dataReader[0]);
                    bus.Code = dataReader[1].ToString();
                    bus.Capacity = Convert.ToInt32(dataReader[2]);
                    bus.Assistant = dataReader[3].ToString();
                    bus.Driver = dataReader[4].ToString();
                    bus.Status = Convert.ToByte(dataReader[5]);
                }

                dataReader.Close();
                command.Dispose();
                return bus;
            }
            catch (Exception ex)
            {
                throw new Exception("No existe el bus.", ex);
            }
            finally
            {
                this.con.Close();
            }

        }

        public Bus SearchById(int Id)
        {
            Bus bus = null;

            this.sql = $"select * from bus where id = '{Id}';";
            this.con = this.connClass.openConnection();
            this.con.Open();
            try
            {
                command = new SqlCommand(sql, this.con);
                dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    bus = new Bus();
                    bus.id = Convert.ToInt32(dataReader[0]);
                    bus.Code = dataReader[1].ToString();
                    bus.Capacity = Convert.ToInt32(dataReader[2]);
                    bus.Assistant = dataReader[3].ToString();
                    bus.Driver = dataReader[4].ToString();
                    bus.Status = Convert.ToByte(dataReader[5]);
                }

                dataReader.Close();
                command.Dispose();
                return bus;
            }
            catch (Exception ex)
            {
                throw new Exception("No existe el bus.", ex);
            }
            finally
            {
                this.con.Close();
            }

        }

    }
}
